import { Button, Col, Dropdown, Row, Menu } from 'antd';
import React from 'react';
import logo from "../../assets/images/calendar.png"
import user from "../../assets/images/user.png"
import "./style.css"
import { LogoutOutlined, SmileOutlined } from '@ant-design/icons';

import { LeftOutlined, RightOutlined, ReloadOutlined, EditOutlined } from '@ant-design/icons';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { curWeekDate, nextWeekDate, preWeekDate } from '../../reudx/slice/DateSlice';
import { format, getWeek, lastDayOfWeek, startOfWeek } from 'date-fns';
import { Link, useNavigate } from 'react-router-dom';
import { onLogoutUser } from '../../reudx/slice/UserSlice';
import { onLogoutUserOfWeek } from '../../reudx/slice/UserOfWeekSlice';

const Navbar: React.FC = () => {
  const currentWeek: Date = useAppSelector(state => state.date.currentWeek)
  const dispatch = useAppDispatch()
  const navigate = useNavigate()
  const nextWeek = () => {
    dispatch(nextWeekDate())
  }
  const preWeek = () => {
    dispatch(preWeekDate())
  }
  const curWeek = () => {
    dispatch(curWeekDate())
  }
  const onLogout = () => {
    localStorage.removeItem("access_token")
    dispatch(onLogoutUser())
    dispatch(onLogoutUserOfWeek())
    navigate("/login")
  }
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <Link to="/">
              Chỉnh sửa
            </Link>
          ),
          icon: <EditOutlined />,
        },
        {
          key: '2',
          label: (
            <Link to="/nhan-vien">
              Nhân viên
            </Link>
          ),
          icon: <SmileOutlined />,
          // disabled: true,
        },
        {
          key: '3',
          danger: true,
          icon: <LogoutOutlined />,
          label: 'Đăng xuất',
          onClick: () => onLogout()
        },
      ]}
    />
  );

  return (
    <div className="navbar">
      <Row align="middle" className="h-full">
        <Col span={4}>
          <div className="flex-center"><Link to="/"> <img src={logo} alt="logo" width={70} /></Link></div>
        </Col>
        <Col span={2}>
          <div className="flex-center">
            <Button onClick={curWeek} type="ghost" color="white" size="large" style={{ color: 'white' }} icon={<ReloadOutlined />}>Tuần này</Button>
          </div>
        </Col>
        <Col span={4}>
          <div className="flex-center">
            <Button onClick={preWeek} type="text" shape="circle" icon={<LeftOutlined style={{ fontSize: 30, color: 'white' }} />}>
            </Button>
            <div className="flex-col-center">
              <h2 className="text-color m-0 pl-5 pr-5">
                Tuần: {getWeek(currentWeek)}
              </h2>
              <h2 className="text-color m-0 pl-10 pr-10">
                <span>{format((startOfWeek(currentWeek, { weekStartsOn: 1 })), "dd/MM")}</span>
                <span> - </span>
                <span>{format((lastDayOfWeek(currentWeek, { weekStartsOn: 1 })), "dd/MM")}</span>
              </h2>
            </div>
            <Button onClick={nextWeek} type="text" shape="circle" icon={<RightOutlined style={{ fontSize: 30, color: 'white' }} />}>
            </Button>
          </div>
        </Col>
        <Col span={2} offset={12}>

          <Dropdown overlay={menu}>
            <a onClick={e => e.preventDefault()}>
              <img src={user} alt="user" />
            </a>
          </Dropdown>
        </Col>
      </Row>
    </div>
  );
};

export default Navbar;