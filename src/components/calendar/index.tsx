import { addDays, format, getWeek, isSameDay, lastDayOfWeek, startOfWeek } from 'date-fns';
import React, { useState } from 'react';
import CellWeek from './CellWeek';
import "./style.css"

const Calendar: React.FC = () => {
    const [currentMonth, setCurrentMonth] = useState<Date>(new Date());
    const [currentWeek, setCurrentWeek] = useState<Number>(getWeek(currentMonth));
    const [selectedDate, setSelectedDate] = useState<Date>(new Date());

  
  return (
    <div className="calendar">
     {/* {renderCells()} */}
      <CellWeek month={currentMonth}/>
    </div>
  );
};

export default Calendar;