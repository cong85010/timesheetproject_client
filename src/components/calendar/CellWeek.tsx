import { Col, Row, Typography } from 'antd';
import { addDays, compareAsc, format, startOfWeek } from 'date-fns';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { TYPE_USER } from '../../constant';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { ticketOfWeek } from '../../reudx/slice/TicketSlice';
import { saveUserOfWeek } from '../../reudx/slice/UserOfWeekSlice';
import ModalTask from '../modal';
import ModalUpdateTask from '../modal/modalUpdate';
import "./style.css";
const { Text, Link } = Typography;

interface props {
    month: Date
}
interface DayOfWeek {
    name: string,
    day: string,
    date: Date
}
const colStyles = {
    minHeight: "calc(100vh - 110px)",
    flexBasis: "14%",
    width: "14%",
    borderLeft: "1px solid #E1E1E1",
};

const Cell: React.FC<DayOfWeek> = (dayOfWeek) => {
    /* REDUX SAVE NOW DAY */
    const { tickets, success, loading } = useAppSelector(state => state.ticket)
    const [ticketsSate, setTicketsState] = useState<any>()
    const day = moment().format('DD-MM-YYYY');
    const dayWeek = moment(dayOfWeek.date).format('DD-MM-YYYY')
    const typeUser = useAppSelector(state => state.user.type)
    const userIdOfWeek: string = useAppSelector(state => state.userOfWeek.userID)
    const userId = useAppSelector(state => state.user.id)

    /* CALL API RENDER */
    useEffect(() => {
        const temp = tickets.filter((t) => t.day == dayOfWeek.day)
        setTicketsState(temp)
    }, [dayOfWeek, success])

    return <div className="cell">
        <div className={`cell__head ${dayWeek == day && "cell__head-active"}`}>
            <Text strong>{dayOfWeek.name} {Number.parseInt(dayOfWeek.day) < 10 ? '0' + dayOfWeek.day : dayOfWeek.day}</Text>
        </div>
        <div className="cell_tasks">
            {
                ticketsSate?.map((values: any) => <ModalUpdateTask {...values} dateSelected={dayOfWeek.date} />)
            }

        </div>
        {
            (typeUser !== TYPE_USER.EMPLOYEE || userId === userIdOfWeek) && moment(dayOfWeek.date).isSameOrAfter(moment(), 'day') && <ModalTask key={dayOfWeek.name} dateSelected={dayOfWeek.date} />
        }

    </div >
}

const CellWeek: React.FC<props> = ({ month }) => {
    const currentWeek: Date = useAppSelector(state => state.date.currentWeek)
    const [arrayDay, setArrayDay] = useState<DayOfWeek[]>([]);
    const userId = useAppSelector(state => state.user.id)
    const dispatch = useAppDispatch()
    const { isNewData } = useAppSelector(state => state.ticket)
    const userIdOfWeek: string = useAppSelector(state => state.userOfWeek.userID)
    useEffect(() => {
        if (isNewData) {
            dispatch(ticketOfWeek({ userId: userIdOfWeek, date: currentWeek }))
        }
    }, [isNewData])
    useEffect(() => {
        const startDate: Date = startOfWeek(currentWeek, { weekStartsOn: 1 });
        const dateFormat: string = "d";
        let day = startDate
        const newArrayDay: any[] = []
        let formattedDate = "";
        const dateFormatName = "EEE";
        for (let i = 0; i < 7; i++) {
            formattedDate = format(day, dateFormat);
            /* Mon - Thus */
            const formattedDateName = format(addDays(startDate, i), dateFormatName)
            newArrayDay.push(
                {
                    name: formattedDateName,
                    day: formattedDate,
                    date: day
                }
            )

            day = addDays(day, 1);
        }
        setArrayDay(newArrayDay)
        if (!userIdOfWeek) {
            dispatch(saveUserOfWeek({ userID: userId as string }))
            dispatch(ticketOfWeek({ userId: userId, date: currentWeek }))
        }
        else {
            dispatch(ticketOfWeek({ userId: userIdOfWeek, date: currentWeek }))
        }
    }, [currentWeek])
    return (
        <div className="cellweek">
            <Row >
                {
                    arrayDay.map((dayOfWeek: DayOfWeek, index: number) => {
                        return <Col style={{ ...colStyles }} key={index} >
                            <Cell {...dayOfWeek} />
                        </Col>
                    })
                }
            </Row>
        </div>
    );
};

export default CellWeek;