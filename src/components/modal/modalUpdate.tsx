import { Button, Col, DatePicker, Form, Input, message, Modal, Popconfirm, Row, Select, Space, Tag, TimePicker, Tooltip, Typography, Upload, UploadProps } from 'antd';
import type { RangePickerProps } from 'antd/es/date-picker';
import React, { useEffect, useState } from 'react';

import { UploadOutlined } from "@ant-design/icons";
import moment from 'moment';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { deleteTicket, TypeTicket, updateTicket } from '../../reudx/slice/TicketSlice';
import { TYPE_USER } from '../../constant';

const { RangePicker } = DatePicker;

const { TextArea } = Input;

const { Text, Link } = Typography;
const { Option } = Select;

const props: UploadProps = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {

  },
};

const disabledDate: RangePickerProps['disabledDate'] = current => {
  // Can not select days before today and today
  return current && current > moment();
};

type TypeProps = {
  ticket: TypeTicket,
  hoursStart: string,
  hoursEnd: string,
  dateSelected: Date

}
type TypePropsStatus = {
  status: string
}
const StatusTask: React.FC<TypePropsStatus> = ({ status }: TypePropsStatus) => {
  return <Tag className="cell_task__tag" color={status === "Do" ? "warning" : status === "Done" ? "success" : "processing"} >{status}</Tag>
}
const ModalUpdateTask: React.FC<TypeProps> = ({ ticket, hoursStart, hoursEnd, dateSelected }: TypeProps) => {
  const [visible, setVisible] = useState(false);
  const dispatch = useAppDispatch()
  const [form] = Form.useForm();
  const userIdOfWeek = useAppSelector(state => state.userOfWeek.userID)
  const typeUser = useAppSelector(state => state.user.type)
  const userId = useAppSelector(state => state.user.id)

  useEffect(() => {
    form.setFieldsValue({
      title: ticket.title,
      description: ticket.description,
      status: ticket.status,
      startTime: moment(ticket.startTime),
      endTime: moment(ticket.endTime)
    })
  }, [ticket.id])
  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (values: any) => {
    const startTime: any = moment(dateSelected)
    const endTime: any = moment(dateSelected)
    startTime.set("hour", moment(values.startTime, 'HH:mm').get('hour')).set("minute", moment(values.startTime, 'HH:mm').get('minute'));
    endTime.set({
      hour: moment(values.endTime, 'HH:mm').get('hour'),
      minute: moment(values.endTime, 'HH:mm').get('minute'),
    })
    const upTicket = {
      ...ticket,
      ...values,
      userID: userIdOfWeek,
      startTime: startTime._d,
      endTime: endTime._d,
    }
    dispatch(updateTicket(upTicket));
    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };
  const confirm = () =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(dispatch(deleteTicket(ticket.id)))
        message.success("Xoá thành công")
        setVisible(false)
      }, 1000);
    });
  // const getUserTicket = (): string => {
  //   if (userId !== ticket.modifiedBy) {
  //     return "Cập nhật bởi " + ticket.modifiedBy
  //   }
  //   if (userId !== ticket.createdBy) {
  //     return "Được tạo bởi " + ticket.createdBy
  //   }

  //   return ""
  // }

  return <>
    <div className="cell_task" onClick={showModal}>
      <Text className="cell_task__title" strong>{ticket.title}</Text>
      <StatusTask status={ticket.status} />
      <Text type="secondary" className="cell_task__description">{ticket.description}</Text>
      <br />
      <Text type="secondary">{hoursStart}</Text>
      <Text type="secondary"> - </Text>
      <Text type="secondary">{hoursEnd}</Text>
      <br />
      <Text type="secondary">Thời gian: <Text type="warning" strong>{ticket.spentTime}</Text></Text>
    </div>
    <Modal
      key={ticket.id}
      title={<h3>Xem công việc</h3>}
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
      okButtonProps={{ disabled: true }}
      cancelButtonProps={{ disabled: true }}
      footer={[
        <Space size="large">
          <Button type="text" key="back" onClick={handleCancel} >
            Huỷ bỏ
          </Button>
          {
            (typeUser === TYPE_USER.ADMIN || typeUser === TYPE_USER.MANAGER || userId === userIdOfWeek) && <>
              <Popconfirm
                title="Xác nhận xoá công việc"
                onConfirm={confirm}
                onVisibleChange={() => console.log('visible change')}
              >
                <Button type="primary" danger key="back" size="large">
                  Xoá
                </Button>

              </Popconfirm>

              <Button type="primary" onClick={form.submit} key="submit" htmlType="submit" size="large" >
                Cập nhật
              </Button></>
          }
        </Space>
      ]}
    >
      <Form
        layout="vertical"
        size="large"
        onFinish={handleOk}
        form={form}
        initialValues={{
          title: ticket.title,
          description: ticket.description,
          status: ticket.status,
          startTime: moment(ticket.startTime),
          endTime: moment(ticket.endTime)
        }}
      >
        <Form.Item label="Trạng thái" name="status" >
          <Select defaultValue={ticket.status || "Do"} style={{ width: 150 }} disabled={!(typeUser === TYPE_USER.ADMIN || typeUser === TYPE_USER.MANAGER || userId === userIdOfWeek)}>
            <Option value="Do">Do</Option>
            <Option value="In Process">In Process</Option>
            <Option value="Done">Done</Option>
          </Select>
        </Form.Item>
        <Form.Item label="Tên công việc" name="title" rules={[{ required: true, message: 'Vui lòng nhập tên công việc !' }]}>
          <Input placeholder="Nhập tên công việc" disabled={!(typeUser === TYPE_USER.ADMIN || typeUser === TYPE_USER.MANAGER || userId === userIdOfWeek)} />
        </Form.Item>
        {/* <Form.Item name="time" label="Thời gian" rules={[{ required: true, message: 'Vui lòng nhập thời gian !' }]}>
          <RangePicker
            size="large"
            showNow
            disabledDate={disabledDate}
            showTime={{ format: 'HH:mm' }}
            format="DD-MM-YYYY HH:mm"
            // defaultValue={[moment(ticket.startTime), moment(ticket.endTime)]}
            // onChange={onChange}
            // onOk={onOk}
            style={{ width: "100%" }}
          />
        </Form.Item> */}
        <Row>
          <Col span="12">
            <Form.Item name="startTime" label="Thời gian bắt đầu" rules={[{ required: true, message: 'Vui lòng nhập thời gian !' }]} >
              <TimePicker format={'HH:mm'} style={{ width: "100%" }} disabled={!(typeUser === TYPE_USER.ADMIN || typeUser === TYPE_USER.MANAGER || userId === userIdOfWeek)} />
            </Form.Item>
          </Col>
          <Col span="12">  <Form.Item name="endTime" label="Thời gian hoàn thành" rules={[{ required: true, message: 'Vui lòng nhập thời gian !' }]}>
            <TimePicker format={'HH:mm'} style={{ width: "100%" }} disabled={!(typeUser === TYPE_USER.ADMIN || typeUser === TYPE_USER.MANAGER || userId === userIdOfWeek)} />
          </Form.Item></Col>
        </Row>
        <Form.Item name="description" label="Mô tả">
          <TextArea
            // value={value}
            // onChange={e => setValue(e.target.value)}
            placeholder="Nhập mô tả công việc"
            autoSize={{ minRows: 3, maxRows: 5 }}
            defaultValue={ticket.description}
            disabled={!(typeUser === TYPE_USER.ADMIN || typeUser === TYPE_USER.MANAGER || userId === userIdOfWeek)}
          />
        </Form.Item>
        <Form.Item name="upload">
          <Upload {...props} className="modal__upload" disabled={!(typeUser === TYPE_USER.ADMIN || typeUser === TYPE_USER.MANAGER || userId === userIdOfWeek)}>
            <Button icon={<UploadOutlined />} className="w-full">Tải lên</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal >
  </>
};

export default ModalUpdateTask;