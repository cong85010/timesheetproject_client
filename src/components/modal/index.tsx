import { Button, Col, DatePicker, Form, Input, Modal, Row, TimePicker, Tooltip, Upload, UploadProps } from 'antd';
import type { RangePickerProps } from 'antd/es/date-picker';
import React, { useState } from 'react';

import { PlusCircleOutlined, UploadOutlined } from "@ant-design/icons";
import moment from 'moment';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { createTicket } from '../../reudx/slice/TicketSlice';
const { RangePicker } = DatePicker;
const { TextArea } = Input;
const props: UploadProps = {
  name: 'file',
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  headers: {
    authorization: 'authorization-text',
  },
  onChange(info) {

  },
};

const disabledDate: RangePickerProps['disabledDate'] = current => {
  // Can not select days before today and today
  return current.isBefore(moment().subtract(1, "day"))
};

const range = (start: number, end: number) => {
  const result = [];
  for (let i = start; i < end; i++) {
    result.push(i);
  }
  return result;
};


type TypeProps = {
  dateSelected: Date
}
const ModalTask: React.FC<TypeProps> = ({ dateSelected }: TypeProps) => {
  const userIdOfWeek = useAppSelector(state => state.userOfWeek.userID)
  const [visible, setVisible] = useState(false);
  const dispatch = useAppDispatch()

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = (values: any) => {

    const startTime: any = moment(dateSelected)
    const endTime: any = moment(dateSelected)
    startTime.set("hour", moment(values.startTime, 'HH:mm').get('hour')).set("minute", moment(values.startTime, 'HH:mm').get('minute'));
    endTime.set({
      hour: moment(values.endTime, 'HH:mm').get('hour'),
      minute: moment(values.endTime, 'HH:mm').get('minute'),
    })
    const ticket = {
      ...values,
      userID: userIdOfWeek,
      startTime: startTime._d,
      endTime: endTime._d,
    }
    dispatch(createTicket(ticket));
    setVisible(false);
  };

  const handleCancel = (e: React.MouseEvent<HTMLElement>) => {
    console.log(e);
    setVisible(false);
  };
  return <>
    <Tooltip title="Tạo công việc" >
      <div className="cell__add pl-10 pr-10" onClick={showModal}>
        <div className="flex-center">
          <Button type="text" shape="circle" size="large" icon={<PlusCircleOutlined />} />
        </div>
      </div>
    </Tooltip>
    <Modal
      title="Tạo công việc"
      visible={visible}
      onOk={handleOk}
      onCancel={handleCancel}
      okButtonProps={{ disabled: true }}
      cancelButtonProps={{ disabled: true }}
      footer={[
        <Button type="text" key="back" onClick={handleCancel} >
          Huỷ bỏ
        </Button>,
        <Button type="primary" form={dateSelected.toString()} key="submit" htmlType="submit" size="large" >
          Thêm mới
        </Button>,
      ]}
    >
      <Form
        layout="vertical"
        size="large"
        id={dateSelected.toString()}
        onFinish={handleOk}
      >
        <Form.Item label="Tên công việc" name="title" rules={[{ required: true, message: 'Vui lòng nhập tên công việc !' }]}>
          <Input placeholder="Nhập tên công việc" />
        </Form.Item>
        <Row>
          <Col span="12">
            <Form.Item name="startTime" label="Thời gian bắt đầu" rules={[{ required: true, message: 'Vui lòng nhập thời gian !' }]} >
              <TimePicker format={'HH:mm'} style={{ width: "100%" }} />
            </Form.Item>
          </Col>
          <Col span="12">  <Form.Item name="endTime" label="Thời gian hoàn thành" rules={[{ required: true, message: 'Vui lòng nhập thời gian !' }]}>
            <TimePicker format={'HH:mm'} style={{ width: "100%" }} />
          </Form.Item></Col>
        </Row>
        <Form.Item name="description" label="Mô tả">
          <TextArea
            // value={value}
            // onChange={e => setValue(e.target.value)}
            placeholder="Nhập mô tả công việc"
            autoSize={{ minRows: 3, maxRows: 5 }}
          />
        </Form.Item>
        <Form.Item name="upload">
          <Upload {...props} className="modal__upload">
            <Button icon={<UploadOutlined />} className="w-full">Tải lên</Button>
          </Upload>
        </Form.Item>
      </Form>
    </Modal>
  </>
};

export default ModalTask;