import { Button, DatePicker, Select, Space, TimePicker, TimePickerProps } from 'antd'
import type { DatePickerProps } from 'antd';
import React, { useEffect, useState } from 'react'
import "./style.css"
import { useAppDispatch, useAppSelector } from '../../hooks';
import { findAllDepartment, findDepartmentById } from '../../reudx/slice/DepartmentSlice';
import { findEmployeesByDepartmentId } from '../../reudx/slice/EmployeeSlice';
import { ticketOfWeek } from '../../reudx/slice/TicketSlice';
import { saveUserOfWeek } from '../../reudx/slice/UserOfWeekSlice';
import { TYPE_USER } from '../../constant';
import { curWeekDate, setWeekDate } from '../../reudx/slice/DateSlice';
const { Option } = Select;

type Props = {
    onlyShowDepartment: boolean
}
export const Sidebar: React.FC<Props> = ({ onlyShowDepartment = false }) => {
    const currentWeek: Date = useAppSelector(state => state.date.currentWeek)
    const { type, departmentId, username, id } = useAppSelector(state => state.user)
    const departments = useAppSelector(state => state.department.departments)
    const employees = useAppSelector(state => state.employees.employees)
    const [valueDefault, setValueDefault] = useState<number | undefined>(undefined)
    const userIDWEEK = useAppSelector(state => state.userOfWeek.userID)
    const dispatch = useAppDispatch()

    useEffect(() => {
        if (type === TYPE_USER.EMPLOYEE || type === TYPE_USER.MANAGER) {
            dispatch(findDepartmentById(departmentId as string))
            dispatch(findEmployeesByDepartmentId(departmentId as string))
        }
        if (type === TYPE_USER.ADMIN) {
            dispatch(findAllDepartment())
        }

    }, [])

    const onChangeDatePicker: DatePickerProps['onChange'] = (date, dateString) => {
        if (date) {
            dispatch(setWeekDate(date?.toDate() as Date))
        } else {
            dispatch(curWeekDate())
        }
    };
    const onChangeDepartment = (departmentId: string) => {
        dispatch(findEmployeesByDepartmentId(departmentId))
    };
    const onChangeEmployee = (index: number) => {
        dispatch(saveUserOfWeek({ userID: employees[index].id as string }))
        dispatch(ticketOfWeek({ userId: employees[index].id, date: currentWeek }))
        setValueDefault(index)
    };
    const onClickDefaultFilter = () => {
        dispatch(saveUserOfWeek({ userID: id as string }))
        dispatch(curWeekDate())
        setValueDefault(undefined)
    };
    const onClickRefreshDepartment = () => {
        dispatch(findEmployeesByDepartmentId(departmentId as string))
    };
    return <div className="sidebar">
        <h2>Timesheet của {userIDWEEK === id ? "Bạn" : employees[valueDefault as number]?.username}</h2>
        {
            type === TYPE_USER.ADMIN ? <div>
                <h3>Danh sách phòng ban</h3>
                <Select size="large" defaultValue="Phòng ban" style={{ width: "100%" }} onChange={onChangeDepartment}>
                    {departments.map((department, index) => department.name !== TYPE_USER.ADMIN && <Option Option key={index} value={department.id} > {department.name}</Option>)}
                </Select>
            </div> :
                departments[0].id !== "-1" && <h3>Phòng ban: {departments[0].name}</h3>
        }

        {
            !onlyShowDepartment ? <>
                <div>
                    <h3>Danh sách nhân viên</h3>
                    <Select
                        size="large"
                        style={{ width: "100%" }}
                        value={valueDefault}
                        onChange={onChangeEmployee}
                    >
                        {employees.map((employee, index) => <Option key={index} value={index}>{employee.full_name} {employee.username === username && "( Là tôi )"}</Option>)}
                    </Select>
                </div>
                <div>
                    <h3>Tìm theo tuần</h3>
                    <DatePicker onChange={onChangeDatePicker} picker="week" size='large' style={{ width: "100%" }} />
                </div>
                {/* <div>
            <h3>Tìm theo giờ</h3>
            <div>
                <TimePicker onChange={onChangeTimePicker} />
                <TimePicker onChange={onChangeTimePicker} />
            </div>
        </div> */}
                <div className="mt-20">
                    <Button type="primary" size="large" style={{ width: "100%" }} onClick={onClickDefaultFilter}>Mặc định</Button>
                </div>

            </> : <Button style={{ width: "100%", marginTop: '20px' }} type="primary" size="large" onClick={onClickRefreshDepartment}>Cập nhật</Button>
        }

    </div >
}