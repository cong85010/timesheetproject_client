import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { RootState } from '../Store';
import { URL_API } from './DateSlice';

export type UserState = {
    id: string | null,
    username: string | null,
    full_name: string | null,
    avatar: string | null,
    token: string | null
    type: string | null,
    departmentId: string | null,
    status: boolean,
    messageError: string | null
    success: boolean,
    loading: boolean,
    error: boolean
}
export interface UserAuth {
    email: string | null,
    username: string | null,
    password: string | null,
}

const initialState: UserState = {
    id: null,
    username: null,
    full_name: null,
    avatar: null,
    token: null,
    type: null,
    departmentId: null,
    status: true,
    messageError: null,
    success: false,
    loading: false,
    error: false
}
export const refreshTokenUser = createAsyncThunk<UserState>("user/refresh", async (_, thunkAPI) => {
    const response = await axios.get(`${URL_API}/users/refresh`)
    const { data } = response.data
    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)
    }
    const decode: UserState = jwt_decode(data.access_token);

    return decode
})
export const loginUser = createAsyncThunk<UserState, UserAuth>("user/login", async (account: UserAuth, thunkAPI) => {
    const response = await axios.post(`${URL_API}/users/login`, { ...account })
    const { data } = response.data
    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)
    }
    const decode: UserState = jwt_decode(data.access_token);
    localStorage.setItem("access_token", JSON.stringify(data.access_token))
    return decode
})
export const loginUserToken = createAsyncThunk<UserState, string>("user/login/token", async (token: string, thunkAPI) => {
    const response = await axios.post(`${URL_API}/users/login/token`, { token: token })

    const { data } = response.data
    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)
    }
    const decode: UserState = jwt_decode(data.access_token);
    localStorage.setItem("access_token", JSON.stringify(data.access_token))

    return decode
})
export const registerUser = createAsyncThunk<UserState, UserAuth>("user/register", async (account: UserAuth, thunkAPI) => {
    const response = await axios.post(`${URL_API}/users/register`, { ...account, full_name: account.username, type: 'EMPLOYEE' })
    const { data } = response.data
    if (data.status === 400) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue((data))
    }
    const decode: UserState = jwt_decode(data.access_token);
    localStorage.setItem("access_token", JSON.stringify(data.access_token))
    return decode
})


export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        onLogoutUser: (state: UserState) => {
            state.id = initialState.id
            state.success = initialState.success
        }
    },
    extraReducers: builder => {
        builder.addCase(loginUser.pending, (state, action) => {
            return { ...state, loading: true, error: false }
        });
        builder.addCase(loginUser.fulfilled, (state, action) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(loginUser.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
        builder.addCase(registerUser.pending, (state, action) => {
            return { ...state, loading: true, error: false }
        });
        builder.addCase(registerUser.fulfilled, (state, action) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(registerUser.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload.message, error: true, loading: false }
        });
        builder.addCase(loginUserToken.pending, (state, action) => {
            return { ...state, loading: true, error: false }
        });
        builder.addCase(loginUserToken.fulfilled, (state, action) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(loginUserToken.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload.message, error: true, loading: false }
        });
        //...
    },
})
export const { onLogoutUser } = userSlice.actions;
export const userSelector = (state: RootState) => state.user

export default userSlice.reducer