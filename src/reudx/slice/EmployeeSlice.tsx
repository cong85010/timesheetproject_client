import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { RootState } from '../Store';
import { URL_API } from './DateSlice';

export type EmployeeState = {
    employees: [{
        id: string | null,
        username: string | null,
        full_name: string | null,
        email: string | null,
        avatar: string | null,
        type: string | null,
        status: boolean,
        departmentId: string | null
    }],
    messageError: string | null
    success: boolean,
    loading: boolean,
    error: boolean
}
const initialState: EmployeeState = {
    employees: [{
        id: null,
        username: null,
        full_name: null,
        email: null,
        avatar: null,
        type: null,
        status: true,
        departmentId: null,
    }],
    messageError: null,
    success: false,
    loading: false,
    error: false
}
export const findEmployeesByDepartmentId = createAsyncThunk<EmployeeState, string>("user/department/id", async (departmentId: string, thunkAPI) => {
    const response = await axios.get(`${URL_API}/users/department/${departmentId}`)
    const { data } = response.data
    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)
    }

    return data
})
export type propsDelete = {
    id: string,
    status: boolean
}

export const deleteUser = createAsyncThunk("user/delete", async ({ id, status }: propsDelete, thunkAPI) => {
    const response = await axios.delete(`${URL_API}/users/${id}`, { params: { status } })
    const { data } = response.data

    if (data.status === 400) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue((response.data.data))
    }
    return data
})

export const saveManager = createAsyncThunk("user/save", async (id: string, thunkAPI) => {
    const response = await axios.post(`${URL_API}/users/department/${id}`)
    const { data } = response.data

    if (data.status === 400) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue((response.data.data))
    }
    return data
})

const employeeSlice = createSlice({
    name: 'employee',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(findEmployeesByDepartmentId.pending, (state, action) => {
            return { ...state, loading: true, success: false, error: false }
        });
        builder.addCase(findEmployeesByDepartmentId.fulfilled, (state, action) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(findEmployeesByDepartmentId.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
        builder.addCase(deleteUser.pending, (state, action) => {
            return { ...state, loading: true, success: false, error: false }
        });
        builder.addCase(deleteUser.fulfilled, (state, action) => {
            return { ...state, success: true, loading: false }
        });
        builder.addCase(deleteUser.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
        builder.addCase(saveManager.pending, (state, action) => {
            return { ...state, loading: true, success: false, error: false }
        });
        builder.addCase(saveManager.fulfilled, (state, action) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(saveManager.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });

    },
})

export default employeeSlice.reducer