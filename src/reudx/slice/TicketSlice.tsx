import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import { format, intervalToDuration } from 'date-fns';
import moment from 'moment';
import { RootState } from '../Store';
import { URL_API } from './DateSlice';

export type TypeTicketCreate = {
    title: string,
    description: string,
    startTime: string,
    endTime: string,
    status: string,
    createdAt: string | null,
    createdBy: string | null,
    modifiedAt: string | null,
    modifiedBy: string | null,
    spentTime: string | null,
    date: string | null,
    userID: string
}
export type TypeTicket = {
    id: string,
    title: string,
    description: string,
    startTime: string,
    endTime: string,
    status: string,
    spentTime: string | null,
    day: string | null,
    hoursStart: string | null,
    hoursEnd: string | null,
    date: string | null,
    userID: string,
    createdAt: string | null,
    createdBy: string | null,
    modifiedAt: string | null,
    modifiedBy: string | null,
}
export type TicketState = {
    tickets: [
        {
            id: string | null,
            title: string | null,
            description: string | null,
            startTime: Date | null,
            endTime: Date | null,
            status: string | null,
            day: string | null,
            hoursStart: string | null,
            hoursEnd: string | null,
            date: string | null,
            userID: string | null,
        }
    ],
    messageError: string | null,
    success: boolean,
    loading: boolean,
    error: boolean
    isNewData: boolean
}
export interface DateTicket {
    userId: string | null,
    date: Date | null,
}
const initialState: TicketState = {

    tickets: [{
        id: null,
        title: '',
        description: '',
        startTime: null,
        endTime: null,
        status: "Do",
        day: null,
        hoursStart: null,
        hoursEnd: null,
        date: null,
        userID: null
    }],
    isNewData: false,
    messageError: null,
    success: false,
    loading: false,
    error: false,
}
export const createTicket = createAsyncThunk<any, TypeTicketCreate>("ticket/add", async (values: TypeTicketCreate, thunkAPI) => {
    const ticket: TypeTicketCreate = {
        title: values.title,
        description: values.description,
        startTime: values.startTime,
        endTime: values.endTime,
        status: "Do",
        createdAt: (new Date()).toISOString(),
        createdBy: values.userID,
        modifiedAt: (new Date()).toISOString(),
        modifiedBy: values.userID,
        userID: values.userID,
        spentTime: getSpentTime(values.startTime, values.endTime),
        date: values.startTime,
    };

    const response = await axios.post(`${URL_API}/ticket`, { ...ticket })

    let { data } = response.data
    console.log(data);

    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)
    }
    return data
})

const getSpentTime = (start: string, end: string) => {
    const date: Duration = intervalToDuration({ start: new Date(start), end: new Date(end) })
    const hour: string = date.hours + "h "
    const minute: string = date.minutes + "m"
    return hour + minute
}
export const updateTicket = createAsyncThunk<any, TypeTicket>("ticket/update", async (values: TypeTicket, thunkAPI) => {

    const ticket: TypeTicket = {
        ...values,
        modifiedAt: (new Date()).toISOString(),
        modifiedBy: values.userID,
        spentTime: getSpentTime(values.startTime, values.endTime),
        date: values.startTime,
    };

    const response = await axios.put(`${URL_API}/ticket`, { ...ticket })

    let { data } = response.data
    console.log(data);

    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)
    }
    return data
})


export const ticketOfWeek = createAsyncThunk<any, DateTicket>("ticket/date", async (dateTicket: DateTicket, thunkAPI) => {
    const dateFormat: string = "d";
    const response = await axios.post(`${URL_API}/ticket/date`, { ...dateTicket })

    let { data } = response.data

    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling

        return thunkAPI.rejectWithValue(data)
    }
    data = data.map((ticket: TypeTicket) => {
        const date = new Date(ticket.startTime)
        const dateEnd = new Date(ticket.endTime)
        const day = Number.parseInt(format(date, dateFormat));
        const hoursStart = format(date, 'p')
        const hoursEnd = format(dateEnd, 'p')
        return {
            day,
            hoursStart,
            hoursEnd,
            ticket
        }
    })
    return { tickets: data }
})

export const deleteTicket = createAsyncThunk("ticket/delete", async (ticketId: string, thunkAPI) => {
    const response = await axios.delete(`${URL_API}/ticket/${ticketId}`)

    let { data } = response.data
    console.log(data);

    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling

        return thunkAPI.rejectWithValue(data)
    }

    return data
})

export const ticketSlice = createSlice({
    name: 'ticket',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(ticketOfWeek.pending, (state, action) => {
            return { ...state, loading: true, error: false, success: false }
        });
        builder.addCase(ticketOfWeek.fulfilled, (state, action: PayloadAction<any>) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(ticketOfWeek.rejected, (state, action: PayloadAction<any>) => {
            return { ...initialState, messageError: action.payload?.message, error: true, loading: false }
        });
        builder.addCase(createTicket.pending, (state, action) => {
            return { ...state, isNewData: false, loading: true, error: false, success: false }
        });
        builder.addCase(createTicket.fulfilled, (state, action: PayloadAction<any>) => {
            return { ...state, isNewData: true, success: true, loading: false }
        });
        builder.addCase(createTicket.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
        builder.addCase(updateTicket.pending, (state, action) => {
            return { ...state, isNewData: false, loading: true, error: false, success: false }
        });
        builder.addCase(updateTicket.fulfilled, (state, action: PayloadAction<any>) => {
            return { ...state, isNewData: true, success: true, loading: false }
        });
        builder.addCase(updateTicket.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
        builder.addCase(deleteTicket.pending, (state, action) => {
            return { ...state, isNewData: false, loading: true, error: false, success: false }
        });
        builder.addCase(deleteTicket.fulfilled, (state, action: PayloadAction<any>) => {
            return { ...state, isNewData: true, success: true, loading: false }
        });
        builder.addCase(deleteTicket.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
    },
})

export const ticketSelector = (state: RootState) => state.user

export default ticketSlice.reducer