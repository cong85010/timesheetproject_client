import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { addWeeks, subWeeks } from 'date-fns';
import { RootState } from '../Store';

export const URL_API: string = "http://localhost:4004/api/v1";

export type DateState = {
    currentWeek: Date,
    messageError: string,
    success: boolean,
    loading: boolean,
    error: boolean
}

const initialState: DateState = {
    currentWeek: new Date(),
    messageError: "Có lỗi xảy ra !!!",
    success: false,
    loading: false,
    error: false,
}


export const dateSlice = createSlice({
    name: 'date',
    initialState,
    reducers: {
        preWeekDate: (state: DateState) => {
            state.currentWeek = subWeeks(state.currentWeek, 1);
        },
        nextWeekDate: (state: DateState) => {
            state.currentWeek = addWeeks(state.currentWeek, 1);
        },
        curWeekDate: (state: DateState) => {
            state.currentWeek = new Date();
        },
        setWeekDate: (state: DateState, action: PayloadAction<Date>) => {
            state.currentWeek = action.payload
        }
    },

})

export const dateSelector = (state: RootState) => state.date

export const { preWeekDate, nextWeekDate, curWeekDate, setWeekDate} = dateSlice.actions
export default dateSlice.reducer