import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { addWeeks, subWeeks } from 'date-fns';
import { RootState } from '../Store';

export const URL_API: string = "http://localhost:4004/api/v1";

export type UserOfWeekState = {
    userID: string,
    messageError: string,
    success: boolean,
    loading: boolean,
    error: boolean
}

const initialState: UserOfWeekState = {
    userID: "",
    messageError: "Có lỗi xảy ra !!!",
    success: false,
    loading: false,
    error: false,
}


export const UserOfWeekSlice = createSlice({
    name: 'userOfWeek',
    initialState,
    reducers: {
        onLogoutUserOfWeek: (state: UserOfWeekState) => {
            state.userID = initialState.userID
            state.success = initialState.success
        },
        saveUserOfWeek: (state: UserOfWeekState, action: PayloadAction<{ userID: string }>) => {
            state.userID = action.payload.userID;
        }
    },

})

export const dateSelector = (state: RootState) => state.date

export const { saveUserOfWeek, onLogoutUserOfWeek } = UserOfWeekSlice.actions
export default UserOfWeekSlice.reducer