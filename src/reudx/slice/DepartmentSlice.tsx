import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import jwt_decode from "jwt-decode";
import { RootState } from '../Store';
import { URL_API } from './DateSlice';

export type DepartmentState = {
    departments: [{
        id: string | null,
        name: string | null,
    }],
    messageError: null,
    success: boolean,
    loading: boolean,
    error: boolean
}

const initialState: DepartmentState = {
    departments: [{
        id: '-1',
        name: "Phòng ban",
    }],
    messageError: null,
    success: false,
    loading: false,
    error: false
}
export const findAllDepartment = createAsyncThunk<DepartmentState>("department", async (_, thunkAPI) => {
    const response = await axios.get(`${URL_API}/department`)
    const { data } = response.data
    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)
    }
    return data
})
export const findDepartmentById = createAsyncThunk<DepartmentState, string>("department/id", async (departmentId: string, thunkAPI) => {
    const response = await axios.get(`${URL_API}/department/${departmentId}`)
    const { data } = response.data
    if (data.status === 400 || data.status === 401) {
        // Return the known error for future handling
        return thunkAPI.rejectWithValue(data)

    }
    data.departments = [data.department]
    delete data.department
    return data
})


export const DepartmentSlice = createSlice({
    name: 'department',
    initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(findAllDepartment.pending, (state, action) => {
            return { ...state, loading: true, error: false }
        });
        builder.addCase(findAllDepartment.fulfilled, (state, action: PayloadAction<any>) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(findAllDepartment.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
        builder.addCase(findDepartmentById.pending, (state, action) => {
            return { ...state, loading: true, error: false }
        });
        builder.addCase(findDepartmentById.fulfilled, (state, action: PayloadAction<any>) => {
            return { ...action.payload, success: true, loading: false }
        });
        builder.addCase(findDepartmentById.rejected, (state, action: PayloadAction<any>) => {
            return { ...state, messageError: action.payload?.message, error: true, loading: false }
        });
    },
})

export default DepartmentSlice.reducer