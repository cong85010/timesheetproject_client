import { configureStore } from '@reduxjs/toolkit'
import DateSlice from './slice/DateSlice'
import DepartmentSlice from './slice/DepartmentSlice'
import employeeSlice from './slice/EmployeeSlice'
import TicketSlice from './slice/TicketSlice'
import UserOfWeekSlice from './slice/UserOfWeekSlice'
import UserSlice from './slice/UserSlice'

export const store = configureStore({
    reducer: {
        user: UserSlice,
        ticket: TicketSlice,
        department: DepartmentSlice,
        date: DateSlice,
        userOfWeek: UserOfWeekSlice,
        employees: employeeSlice
    }, middleware: getDefaultMiddleware =>
        getDefaultMiddleware({
            serializableCheck: false,
        })
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch