import React from 'react';
import './App.css';
import { Col, Row } from 'antd';
import Navbar from './components/navbar';
import { Sidebar } from './components/sidebars';
import Calendar from './components/calendar';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Row>
        <Col span={4}>
          <Sidebar onlyShowDepartment={false} />
        </Col>
        <Col span={20}>
          <Calendar />
        </Col>
      </Row>
    </div>
  );
}

export default App;
