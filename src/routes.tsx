import { message } from 'antd';
import jwt_decode from 'jwt-decode';
import React, { useEffect } from 'react';
import {
  Navigate, Route, Routes, useNavigate
} from "react-router-dom";
import App from './App';
import './App.css';
import { useAppDispatch, useAppSelector } from './hooks';
import Login from './page/auth/login';
import Register from './page/auth/register';
import Employees from './page/employees';
import { loginUserToken, UserState } from './reudx/slice/UserSlice';

function PrivateRoute({ children }: any) {
  const dispatch = useAppDispatch()
  let navigate = useNavigate();

  const { success, error, messageError, loading }: UserState = useAppSelector(state => state.user)
  useEffect(() => {
    if (success) {
      navigate("/")
    } else {
      navigate("/login")
    }
    if (error) {
      message.error(messageError);
    }
    if (!success && !loading) {
      const token = localStorage.getItem("access_token")
      if (token) {
        dispatch(loginUserToken(JSON.parse(token)));
      }
    }
  }, [success, error, loading])
  if (!success) {
    return <Navigate to="/login" />;
  } else {
    return children;
  }
}
const RoutesApp: React.FC = () => {


  return (
    <Routes>
      <Route path="/login" element={<Login />}></Route>
      <Route path="/register" element={<Register />}></Route>
      <Route path="/" element={
        <PrivateRoute>
          <App />
        </PrivateRoute>}>
      </Route>
      <Route path="/nhan-vien" element={
        <PrivateRoute>
          <Employees />
        </PrivateRoute>}>
      </Route>
      <Route path="*" element={<Navigate to="/" replace />} />

    </Routes>
  );
}

export default RoutesApp;
