import React from 'react';
import ReactDOM from 'react-dom/client';
import reportWebVitals from './reportWebVitals';
import 'antd/dist/antd.css';
import './index.css';
import { BrowserRouter } from 'react-router-dom';
import RoutesApp from './routes';
import { Provider } from 'react-redux';
import { store } from './reudx/Store';


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <BrowserRouter>
    <Provider store={store}>
      <RoutesApp />
    </Provider>
  </BrowserRouter>
);
reportWebVitals()