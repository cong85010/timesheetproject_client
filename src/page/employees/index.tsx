import { Col, message, Popconfirm, Row, Space, Table, Tag } from 'antd';
import Button from 'antd/es/button';
import type { ColumnsType } from 'antd/lib/table';
import React, { useEffect, useState } from 'react';
import Navbar from '../../components/navbar';
import { Sidebar } from '../../components/sidebars';
import { TYPE_USER } from '../../constant';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { findAllDepartment } from '../../reudx/slice/DepartmentSlice';
import { deleteUser, findEmployeesByDepartmentId, saveManager } from '../../reudx/slice/EmployeeSlice';

interface DataType {
    key: string;
    id: string | null;
    username: string | null;
    full_name: string | null;
    avatar: string | null;
    type: string | null;
    departmentId: string | null;
}


const Employees: React.FC = () => {
    const { employees, success } = useAppSelector(state => state.employees)
    const [departmentId, setDepartmentId] = useState<string>()
    const dispatch = useAppDispatch()

    const confirm = (id: string, status: boolean) =>
        new Promise(resolve => {
            setTimeout(() => {
                resolve(dispatch(deleteUser({ id, status })));
                message.success("Thành công")
            }, 1000);
        });
    const onChangeManager = (id: string) => {
        return new Promise(resolve => {
            setTimeout(() => {
                resolve(dispatch(saveManager(id)))
                message.success("Thành công")
            }, 1000);
        });
    }
    useEffect(() => {
        dispatch(findAllDepartment())
        dispatch(findEmployeesByDepartmentId(departmentId as string))
    }, [success])
    const columns: ColumnsType<any> = [
        {
            title: 'Full Name',
            dataIndex: 'full_name',
            key: 'name',
            render: text => <a>{text}</a>,
        },
        {
            title: 'Username',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Loại',
            key: 'type',
            dataIndex: 'type',
            render: (_, { type }) => {
                let color = "geekblue"
                if (type === TYPE_USER.MANAGER) {
                    color = 'volcano';
                }
                return (
                    <Tag color={color} key={type}>
                        {type}
                    </Tag>
                );
            }
        },
        {
            title: 'Trạng thái',
            key: 'status',
            dataIndex: 'status',
            render: (_, { status }) => {

                let color = "success"
                if (!status) {
                    color = 'volcano';
                }
                return (
                    <Tag color={color} key={status}>
                        {status ? "Đang làm" : "Đã nghỉ việc"}
                    </Tag>
                );
            }
        },
        {
            title: 'Action',
            key: 'del',
            width: 100,
            render: (_, { id, status }) => (
                <Popconfirm
                    title="Xác nhận"
                    onConfirm={() => confirm(id, status)}
                    onVisibleChange={() => console.log('visible change')}
                >
                    {
                        status ? <Button type="ghost" danger >Nghỉ việc</Button> :
                            <Button type="ghost" style={{ backgroundColor: 'green', color: 'white' }} >Khôi phục</Button>
                    }

                </Popconfirm>
            ),
        },
        {
            title: 'Quản lý',
            key: 'manage',
            width: 100,
            render: (_, { id, type }) => {
                if (type !== TYPE_USER.MANAGER) {
                    return <Space size="middle" onClick={() => onChangeManager(id)}>
                        <Button type="primary">Phân công</Button>
                    </Space>
                }
                return <Space size="middle">
                    <Button type="dashed">Quản lý</Button>
                </Space>
            }
        },
    ];

    return <>
        <Navbar />
        <Row>
            <Col span={4} className="pl-10 pr-10">
                <Sidebar onlyShowDepartment={true} />
            </Col>
            <Col span={20}>
                <h2>Danh sách nhân viên</h2>
                <Table columns={columns} dataSource={employees[0].id ? employees : []} /></Col>
        </Row>
    </>
};

export default Employees;