import { Button, Checkbox, Col, Form, Input, message, Row, Space } from 'antd';
import React, { useEffect } from 'react';
import "./style.css";
import User from "../../assets/images/userLogin.png"
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import { loginUser, UserAuth, userSelector, UserState } from '../../reudx/slice/UserSlice';
import { useAppDispatch, useAppSelector } from '../../hooks';

const Login: React.FC = () => {
  const { success, error, messageError, token }: UserState = useAppSelector(userSelector)
  let navigate = useNavigate();

  const dispatch = useAppDispatch()
  const onFinish = (values: UserAuth) => {
    console.log('Success:', values);
    dispatch(loginUser(values));
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  useEffect(() => {
    if (success) {
      navigate("/")
      message.success('Đăng nhập thành công');
    } else {
      navigate("/login")
    }
    if (error) {
      message.error(messageError);
    }
  }, [success, error])

  return (
    <div className="auth">
      <Row >
        <Col span={24}><h2 className="flex-center mt-20 mb-20">WELCOME</h2></Col>
        <Col span={12}>
          <div className="flex-col-center h-full">
            <div className="auth__bg-circle flex-center"><img src={User} alt="avatat" /></div>
            <h3 className="mt-20 mb-20">Calendar</h3>
          </div>
        </Col>
        <Col span={12}>
          <h2 className="mt-20 mb-20 text-center">Đăng Nhập</h2>
          <div className="auth__form">
            <Form
              name="normal_login"
              className="auth-form"
              size="large"
              initialValues={{ remember: true }}
              onFinish={onFinish}
            >
              <Form.Item
                name="username"

                rules={[{ required: true, message: 'Please input your Username!' }]}
              >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[{ required: true, message: 'Please input your Password!' }]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item>
                <Button type="primary" htmlType="submit" className="login-form-button w-full">
                  Đăng nhập
                </Button>
              </Form.Item>
              <div className="text-end">
                <strong>Chưa có tài khoản?</strong> <br />
                <Link to="/register">Đăng ký ngay</Link>
              </div>
            </Form>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Login;