import { Button, Checkbox, Col, Form, Input, message, Row, Select, Space } from 'antd';
import React, { useEffect } from 'react';
import "./style.css";
import User from "../../assets/images/userLogin.png"
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import { registerUser, UserAuth, userSelector, UserState } from '../../reudx/slice/UserSlice';
import { useAppDispatch, useAppSelector } from '../../hooks';
import { findAllDepartment } from '../../reudx/slice/DepartmentSlice';
import { TYPE_USER } from '../../constant';
const { Option } = Select;

const Register: React.FC = () => {
  const departments = useAppSelector(state => state.department.departments)
  const { success, error, loading, messageError }: UserState = useAppSelector(userSelector)
  let navigate = useNavigate();

  const dispatch = useAppDispatch()
  const onFinish = (values: UserAuth) => {
    console.log('Success:', values);
    dispatch(registerUser(values));
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  useEffect(() => {
    dispatch(findAllDepartment())
  }, [])
  useEffect(() => {
    if (success) {
      navigate("/")
      message.success('Đăng ký thành công');
    }
    if (error) {
      message.error(messageError);
    }
  }, [success, error])
  return (
    <div className="auth">
      <Row >
        <Col span={24}><h2 className="flex-center mt-20">WELCOME</h2></Col>
        <Col span={12}>
          <div className="flex-col-center h-full">
            <div className="auth__bg-circle flex-center"><img src={User} alt="avatat" /></div>
            <h3 className="mt-20 mb-20">Calendar</h3>
          </div>
        </Col>
        <Col span={12}>
          <h2 className="text-center">Đăng Ký</h2>
          <div className="auth__form">
            <Form
              name="normal_login"
              className="auth-form"
              size="large"
              initialValues={{ remember: true }}
              onFinish={onFinish}
            >
              <Form.Item
                name="email"
                rules={[
                  {
                    type: 'email',
                    message: 'Vui lòng nhập email',
                  },
                  {
                    required: true,
                    message: 'Vui lòng nhập email',
                  },
                ]}
              >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
              </Form.Item>

              <Form.Item
                name="username"
                rules={[{ required: true, message: 'Vui lòng nhập username' }]}
              >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập mật khẩu!',
                  },
                ]}
                hasFeedback
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Password"
                />
              </Form.Item>
              <Form.Item
                name="confirm"
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng nhập xác nhận mật khẩu',
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Không trùng mật khẩu'));
                    },
                  }),
                ]}
              >
                <Input
                  prefix={<LockOutlined className="site-form-item-icon" />}
                  type="password"
                  placeholder="Mật khẩu"
                />
              </Form.Item>
              <Form.Item
                name="department"
                rules={[
                  {
                    required: true,
                    message: 'Vui lòng chọn phòng ban',
                  },
                  () => ({
                    validator(_, value) {
                      if (!value || value !== "Phòng ban") {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error('Vui lòng chọn phòng ban'));
                    },
                  }),
                ]}
              >
                <Select defaultValue="Phòng ban" style={{ width: "100%" }}>
                  {departments.map((department, index) => department.name !== TYPE_USER.ADMIN && <Option key={index} value={department.id}>{department.name}</Option>)}
                </Select>
              </Form.Item>
              <Form.Item>
                <Button loading={loading} type="primary" htmlType="submit" className="login-form-button w-full">
                  Đăng ký
                </Button>
              </Form.Item>
              <div className="text-end">
                <strong>Đã có tài khoản?</strong> <br />
                <Link to="/login">Đăng nhập ngay</Link>
              </div>
            </Form>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Register;